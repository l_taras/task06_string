package task1;

import java.util.Locale;

public class Main {
    private Main() {
    }

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.start();
        Locale locale = Locale.getDefault();
    }
}