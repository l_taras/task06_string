package task1;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {
    private Map<String, String> menus;
    private Locale locale;
    private ResourceBundle bundle;

    protected Menu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setMenu() {
        menus = new HashMap<>();
        menus.put("1", bundle.getString("1"));
        menus.put("2", bundle.getString("2"));
        menus.put("3", bundle.getString("3"));
        menus.put("4", bundle.getString("4"));
        menus.put("5", bundle.getString("5"));
        menus.put("6", bundle.getString("6"));
        menus.put("7", bundle.getString("7"));
    }

    private void menuEn() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void menuDe() {
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void menuUk() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void show() {
        for (String string : menus.keySet()) {
            System.out.println(menus.get(string));
        }
    }

    protected void start() {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        do {
            show();
            System.out.println("Please, enter menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case ("1"): {
                        System.out.println(StringUtils.stringUtils(getInput("Enter for String Utils")));
                        break;
                    }
                    case ("2"): {
                        String temp = getInput("Enter sentence for checking");
                        System.out.println("is sentence? " + checkForSentance(temp));
                        break;
                    }
                    case ("3"): {
                        String temp = getInput("Enter sentence for split");
                        System.out.println(splitWords(temp, " "));
                        break;
                    }
                    case ("4"): {
                        String temp = getInput("Enter sentence");
                        System.out.println(replaceVowels(temp, "_"));
                        break;
                    }
                    case ("5"): {
                        menuDe();
                        break;
                    }
                    case ("6"): {
                        menuUk();
                        break;
                    }
                    case ("7"): {
                        menuEn();
                        break;
                    }
                }

            } catch (Exception e) {
                System.err.println("Incorrect input");
            }
        } while (!keyMenu.equals("Q"));
    }


    private String replaceVowels(String text, String replacement) {
        String regex = "[AEIOUaeiou]";
        Pattern p2 = Pattern.compile(regex);
        Matcher m2 = p2.matcher(text);
        return m2.replaceAll(replacement);
    }

    private String getInput(String a) {
        System.out.println(a);
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        text = text.trim();
        while (text.contains("  ")) {
            text = text.replaceAll("  ", " ");
        }
        return text;
    }


    private List<String> splitWords(String text, String split) {
        text = text.trim();
        while (text.contains("  ")) {
            text = text.replaceAll("  ", " ");
        }
        List<String> output = Arrays.asList(text.split(split));
        return output;
    }

    private boolean checkForSentance(String text) {
        String regex = "^[A-Z]+.*[\\.]$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        return m.matches();
    }
}


