package task1;

public class StringUtils {
    private StringUtils() {
    }

    public static String stringUtils(Object... list) {
        StringBuilder str = new StringBuilder();
        for (Object o : list) {
            str.append(o.toString() + " ");
        }
        return str.toString();

    }
}


