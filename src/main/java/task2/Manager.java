package task2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Manager {
    private String inputText = "";
    private File file;
    private List<Sentence> text = new LinkedList<>();

    public Manager(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public List<Sentence> getText() {
        return text;
    }

    public static void main(String[] args) throws IOException {
        File file = new File("src\\main\\resources\\text.txt");
        if (!file.exists()) {
            System.out.println("File is not found");
            System.exit(1);
        }

        Manager manager = new Manager(file);
        manager.readTextFromFile();
        manager.replace();
        System.out.println(manager.inputText);
        System.out.println();
        manager.splitForSentance();
        System.out.println();

//        for (Sentence sentence : manager.text) {
//            for (Word word : sentence.getWords()) {
//                System.out.println(" word= " + word);
//            }
//            for (Punctuation point : sentence.getPoints()) {
//                System.out.println(" point= " + point);
//            }
//
//        }
        manager.sortByWords();
//        for (Sentence s : manager.getText()) {
//            System.out.println(s);
//
//        }
    }

    private void readTextFromFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String s;
        while ((s = br.readLine()) != null) {
            inputText += s.trim();
        }
        br.close();
    }

    private void replace() {
        String regex = "  *|[^:print:]+]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(inputText);
        m.reset();
        inputText = m.replaceAll(" ");
    }

    private void splitForSentance() {
        String regex = "[^\\.\\!\\?]*[\\.\\!\\?]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(inputText);
        m.reset();
        while (m.find()) {
            Sentence s = new Sentence(inputText.substring(m.start(), m.end()));
            text.add(s);
        }

    }
    //Task1
    private void equalWords () {

    }
    //Task2
    private void sortByWords() {
        text.stream().sorted((x, y) -> x.getWords().size() - y.getWords().size()).forEach(System.out::println);
    }

}

