package task2;

import java.util.StringJoiner;

public class Punctuation {
    String point;

    public Punctuation(String point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Punctuation.class.getSimpleName() + "[", "]")
                .add("point='" + point + "'")
                .toString();
    }
}

