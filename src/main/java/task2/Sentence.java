package task2;

import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
    String sentence;
    List<Word> words = new LinkedList<>();
    List<Punctuation> points = new LinkedList<>();

    public Sentence(String sentence) {
        this.sentence = sentence;
        splitForWord();
        splitForNotWord();
    }

    private void splitForWord() {
        String regex = "[\\w]+";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(sentence);
        m.reset();
        while (m.find()) {
            Word w = new Word(sentence.substring(m.start(), m.end()));
            words.add(w);
        }
    }

    private void splitForNotWord() {
        String regex = "[^\\w|\\s]+";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(sentence);
        m.reset();
        while (m.find()) {
            Punctuation point = new Punctuation(sentence.substring(m.start(), m.end()));
            points.add(point);
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Sentence.class.getSimpleName() + "[", "]")
                .add("sentence='" + sentence + "'")
                .add("words=" + words)
                .add("points=" + points)
                .toString();
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public List<Punctuation> getPoints() {
        return points;
    }

    public void setPoints(List<Punctuation> points) {
        this.points = points;
    }
}
