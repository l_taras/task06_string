package task2;

import java.util.StringJoiner;

public class Word {
    String chars;

    public Word(String chars) {
        this.chars = chars;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Word.class.getSimpleName() + "[", "]")
                .add("chars='" + chars + "'")
                .toString();
    }
}
